module Pizzeria.klient {
    requires javafx.controls;
    requires javafx.fxml;
    requires com.fasterxml.jackson.databind;
    requires httpclient;
    requires httpcore;
    requires com.fasterxml.jackson.core;

    opens edu.pwsztar to javafx.fxml;
    opens edu.pwsztar.UI to javafx.fxml;
    opens edu.pwsztar.dto to com.fasterxml.jackson.databind;

    exports edu.pwsztar.UI;
    exports edu.pwsztar.dto;
    exports edu.pwsztar;
}
