package edu.pwsztar;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;


/**
 * Główna klasa aplikacji
 */
public class App extends Application {

    /**
     * Scena JavaFX
     */
    private static Scene scene;

    /**
     * Funkcja inicjująca GUI
     * @param stage Domyślny parametr wymuszony przez interfejs
     * @throws Exception W przypadku błędów w ładowaniu pliku FXML
     */
    @Override
    public void start(Stage stage) throws Exception {

        FXMLLoader loader = new FXMLLoader(App.class.getResource("/start.fxml"));

        scene = new Scene(loader.load(), 800, 600);

        stage.setScene(scene);
        stage.show();
    }

    /**
     * Funkcja zwracająca obiekt sceny
     * @return obiekt klasy Scene
     */
    public static Scene getScene() {
        return scene;
    }

    /**
     * Funkcja main
     * @param args Argumenty przekazywane podczas uruchamiania
     */
    public static void main(String[] args) {
        launch();
    }
}
