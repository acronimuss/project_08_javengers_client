package edu.pwsztar.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.pwsztar.dto.ProductDto;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * Klasa odpowiedzialna za komunikację z serwerem
 */
public class ProductService {
    /**
     * Adres z którego można pobrać listę dostępnych produktów
     */
    final private String productsURL = "http://localhost:8081/produkty/pobierz";
    /**
     * Adres na który można wysłać zamówienie, otrzymując przy tym plik pdf zawierający paragon
     */
    final private String orderURL = "http://localhost:8081/produkty/zamow";

    /**
     * Obiekt służący do konwersji Obiekt->JSON oraz JSON->Obiekt
     */
    final private ObjectMapper mapper;

    /**
     * Domyślny konstruktor
     */
    public ProductService(){
        mapper = new ObjectMapper();
    }

    /**
     * Funkcja pobierająca tablicę obiektów klasy ProductDto z serwera
     * @return Zwraca tablicę obiektów ProductDto
     * @throws IOException W przypadku błędów podczas łączenia z serwerem
     */
    public ProductDto[] getProducts() throws IOException {

        HttpClient client;
        client = HttpClientBuilder.create().build();

        HttpGet getRequest = new HttpGet(productsURL);
        HttpEntity result = client.execute(getRequest).getEntity();
        String json = EntityUtils.toString(result);

        return mapper.readValue(json, ProductDto[].class);
    }

    /**
     * Funkcja wysyłająca zamówienie do serwera, jednocześnie zwracająca paragon otrzymany w odpowiedzi
     * @param products Tablica obiektów klasy ProductDto
     * @return Tablica bajtów zawierająca wnętrze pliku pdf - paragonu
     * @throws IOException W przypadku błędów w komunikacji z serwerem
     * @throws Error W przypadku błędów konwersji Obiekt->JSON
     */
    public byte[] sendProducts(ArrayList<ProductDto> products) throws IOException, Error {

        CloseableHttpClient client = HttpClients.createDefault();
        try{

            String json = mapper.writeValueAsString(products);

            HttpPost postRequest = new HttpPost(orderURL);
            postRequest.setEntity(new StringEntity(json));
            postRequest.setHeader("Accept", "application/json");
            postRequest.setHeader("Content-type", "application/json");

            HttpResponse response = client.execute(postRequest);


            if(response.getStatusLine().getStatusCode() != 200)
                throw new IOException("Błąd serwera... ");


            byte[] receipt = response.getEntity().getContent().readAllBytes();

            client.close();

            return receipt;
        }
        catch(JsonProcessingException | UnsupportedEncodingException e){
            throw new Error("Internal error");
        }


    }
}
