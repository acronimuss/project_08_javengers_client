package edu.pwsztar;

/**
 * Klasa inicjująca
 */
public class Main {


    /**
     * Funkcja uruchamiająca aplikację
     * @param args Argumenty przekazywane podczas uruchamiania programu
     */
    public static void main(String[] args){
        App.main(args);
    }
}
