package edu.pwsztar.UI;

import edu.pwsztar.App;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.*;

/**
 * Kontroler ekranu końcowego
 */
public class EndController {

    /**
     * Napis służący do wyświetlania komunikatów
     */
    @FXML
    Label header;

    /**
     * Tablica bajtów przechowująca plik pdf z paragonem
     */
    private byte[] receipt;

    /**
     * Funkcja inicjująca
     * @param receipt wnętrze pliku pdf - paragonu
     */
    public void init(byte[] receipt){




        this.receipt = receipt;
    }

    /**
     * Funkcja umożliwiająca wyjście z programu
     */
    @FXML
    private void exit(){

        ((Stage)App.getScene().getWindow()).close();
    }

    /**
     * Funkcja wywoływana gdy użytkownik wyrazi chęć zapisu paragonu
     */
    @FXML
    private void saveReceipt()  {

        FileChooser fc = new FileChooser();
        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("PDF files","*.pdf"));
        File f = fc.showSaveDialog(null);


        if(f != null){
            try{

                FileOutputStream writer = new FileOutputStream(f.getAbsolutePath());
                writer.write(receipt);
                writer.close();
            }catch(IOException e){
                header.setText("Błąd podczas zapisu paragonu...");
                return;
            }

        }

        header.setText("Paragon zapisany pomyślnie!");

    }

}
