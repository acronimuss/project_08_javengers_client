package edu.pwsztar.UI;

import edu.pwsztar.App;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;

import java.io.IOException;

/**
 * Kontroler ekranu powitalnego
 */
public class StartController {

    /**
     * Funkcja wywoływana gdy użytkownik postanawia przejść do kolejnego ekranu
     * @throws IOException W przypadku błędów podczas ładowania pliku FXML
     */
    @FXML
    private void next() throws IOException {


        FXMLLoader loader = new FXMLLoader(App.class.getResource( "/order.fxml"));
        App.getScene().setRoot(loader.load());
        ((orderController)loader.getController()).init();
    }
}
