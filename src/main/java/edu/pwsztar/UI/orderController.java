package edu.pwsztar.UI;

import edu.pwsztar.App;
import edu.pwsztar.dto.ProductDto;
import edu.pwsztar.service.ProductService;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Bounds;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

import java.io.IOException;
import java.util.ArrayList;


public class orderController {


    /**
     * Obiekt klasy ProductService używany do komunikacji z serwerem
     */
    private ProductService service;

    /**
     * Napis tytułowy, służy do wyświetlania komunikatów
     */
    @FXML
    Label header;

    /**
     * Napis w którym wypisany zostaje całkowity koszt zakupu
     */
    @FXML
    Label priceLabel;

    /**
     * Główny obszar w którym znajdują się interaktywne kontrolki
     */
    @FXML
    AnchorPane mainPane;

    /**
     * Obszar w którym wyświetlane są produkty typu Zestaw
     */
    @FXML
    FlowPane setsFlow;

    /**
     * Obszar w którym wyświetlane są produkty typu Pizza
     */
    @FXML
    FlowPane pizzaFlow;

    /**
     * Obszar w którym wyświetlane są produkty typu Napój
     */
    @FXML
    FlowPane drinksFlow;

    /**
     * Obszar w którym wyświetlane są wybrane przez użytkownika produkty - koszyk
     */
    @FXML
    VBox basketPane;

    /**
     * Przycisk usuwający wszystkie elementy z koszyka
     */
    @FXML
    Button clearButton;

    /**
     * Przycisk wykorzystywany przez użytkownika do podejmowania decyzji o zakupie
     */
    @FXML
    Button orderButton;


    /**
     * Margines "pudełek" prezentujących produkty
     */
    private final Insets boxMargin = new Insets(10, 10, 10, 10);
    /**
     * Padding "pudełek" prezentujących produkty
     */
    private final Insets boxPadding = new Insets(5, 5, 5, 5);
    /**
     * Obramowanie "pudełek" prezentujących produkty
     */
    private final Border boxBorder = new Border(
                new BorderStroke(Color.LIGHTGREY,
                BorderStrokeStyle.SOLID,
                CornerRadii.EMPTY,
                BorderWidths.DEFAULT));
    /**
     * Zmienna przechowująca informację o stanie programu podczas przeciągania produktu
     */
    private boolean dragging = false;

    /**
     * "Pudełko" wyświetlane podczas przeciągania produktu
     */
    private VBox dragBox = null;

    /**
     * Zmienna przechowująca informację o stanie programu podczas wyboru darmowych napoi
     */
    private boolean choosingFreeDrinks = false;
    /**
     * Zmienna przechowująca ilość przysługujących użytkownikowi darmowych napoi
     */
    private int freeDrinksLeft = 0;

    /**
     * Metoda inicjująca, odpowiada za wczytanie i wyświetlenie produktów po otwarciu tego ekranu
     */
    public void init(){



        service = new ProductService();

        Platform.runLater(this::loadProducts);
    }

    /**
     * Metoda obsługująca zdarzenia związane z "pudełkami" wyświetlającymi produkty
     * @param event zdarzenie typu MouseEvent
     */
    private void boxHandler(MouseEvent event){

        VBox box = ((VBox)event.getSource());

        ProductDto dto = (ProductDto)(box.getUserData());


        if(event.getEventType() == MouseEvent.MOUSE_ENTERED){
            box.setStyle("-fx-background-color: TRANSPARENT");
        }

        else if(event.getEventType() == MouseEvent.MOUSE_EXITED)
            box.setStyle("-fx-background-color: WHITE");

        else if(event.getEventType() == MouseEvent.MOUSE_CLICKED) {


            if(!dragging)

                addToBasket(dto);
        }

        else if(event.getEventType() == MouseEvent.DRAG_DETECTED) {

            dragging = true;

            box.startFullDrag();
        }

        else if(event.getEventType() == MouseEvent.MOUSE_DRAGGED){

            dragBox.setDisable(false);
            dragBox.setVisible(true);
            dragBox.setTranslateX(event.getSceneX()-(dragBox.getWidth()/2));
            dragBox.setTranslateY(event.getSceneY()-(dragBox.getHeight()/2));
        }

        else if(event.getEventType() == MouseEvent.MOUSE_RELEASED && dragging){

            dragging = false;

            double dbx = dragBox.getTranslateX()+(dragBox.getWidth()/2);
            double dby = dragBox.getTranslateY()+(dragBox.getHeight()/2);

            if(dto.getType().equals("pizza") ){

                for(Node temp : pizzaFlow.getChildren()) {

                    VBox vbox = (VBox) temp;

                    ProductDto tempDto = (ProductDto) (vbox.getUserData());

                    Bounds screenBounds = vbox.localToScene(vbox.getBoundsInLocal());


                    if (screenBounds.contains(dbx, dby) && vbox!=box) {

                        ProductDto pizza5050 = new ProductDto(-1,
                                dto.getName() + "/ " + tempDto.getName(),
                                "5050",
                                "",
                                tempDto.getPrice() / 2 + dto.getPrice() / 2,
                                1,
                                dto.getId(),
                                tempDto.getId());

                        addToBasket(pizza5050);
                    }
                }
            }


            dragBox.setTranslateX(0);
            dragBox.setTranslateY(0);
            dragBox.setVisible(false);
            dragBox.setDisable(true);
        }

    }

    /**
     * Metoda obsługująca zdarzenia związane z "pudełkami" wyświetlającymi produkty podczas wybierania darmowych napoi
     * @param event zdarzenie typu MouseEvent
     */
    private void freeDrinkHandler(MouseEvent event){



        VBox box = ((VBox)event.getSource());
        ProductDto dto = (ProductDto)(box.getUserData());


        if(event.getEventType() == MouseEvent.MOUSE_ENTERED){
            box.setStyle("-fx-background-color: TRANSPARENT");
        }

        else if(event.getEventType() == MouseEvent.MOUSE_EXITED)
            box.setStyle("-fx-background-color: WHITE");

        else if(event.getEventType() == MouseEvent.MOUSE_CLICKED) {

            if(freeDrinksLeft>0){


                addToBasket(new ProductDto(dto.getId(),
                        dto.getName(),
                        "darmowynapoj",
                        dto.getDescription(),
                        0.0,
                        dto.getAmount(),
                        dto.getOptional1(),
                        dto.getOptional2()));

                freeDrinksLeft--;

                header.setText("Wybierz darmowe napoje, pozostało ci: "+freeDrinksLeft);
            }

        }
    }

    /**
     * Funkcja tworząca napis opisujący cenę produktu ze zmiennej typu double
     * @param price cena jako zmienna typu double
     * @return Napis opisujący cenę produktu
     */
    private String createPriceStr(double price){


        double priceNum = (Math.round(price * 100.0) / 100.0);
        return String.format("%.2f", priceNum)+"zł";
    }

    /**
     * Funkcja pomocnicza tworząca obiekt typu Label - napis wyświetlany na ekranie
     * @param text Wyświetlany napis jako String
     * @param size Wielkość tekstu
     * @return Obiekt klasy Label
     */
    private Label createLabel(String text, int size){

        Label label = new Label();

        label.setText(text);

        label.setFont(new Font("System", size));

        return label;
    }

    /**
     * Funkcja pomocnicza tworząca "Pudełko" prezentujące produkt
     * @param dto Obiekt klasy ProductDto, opisujący produkt
     * @return Obiekt klasy VBox, prezentujący produkt
     */
    private VBox createBox(ProductDto dto){




        VBox box = new VBox();

            FlowPane.setMargin(box, boxMargin);

            box.setPrefSize(235, 235);

            box.setAlignment(Pos.TOP_CENTER);

            box.setPadding(boxPadding);

            box.setBorder(boxBorder);

            box.setOnMouseEntered(this::boxHandler);
            box.setOnMouseExited(this::boxHandler);
            box.setOnMouseClicked(this::boxHandler);
            box.setOnDragDetected(this::boxHandler);
            box.setOnMouseDragged(this::boxHandler);
            box.setOnMouseReleased(this::boxHandler);

            box.setUserData(dto);

            box.setStyle("-fx-background-color: WHITE");

            box.setCursor(Cursor.HAND);

        Label name = createLabel(dto.getName(), 20);

            VBox.setMargin(name, new Insets(0,0,10,0));

        Label desc = createLabel(dto.getDescription(), 15);

            VBox.setMargin(desc, new Insets(0,0,10,0));


            desc.setWrapText(true);

        Label price = createLabel(createPriceStr(dto.getPrice()), 15);
            VBox.setMargin(price, new Insets(30,0,0,0));


        box.getChildren().add(name);
        box.getChildren().add(desc);
        box.getChildren().add(price);

        return box;
    }

    /**
     * Funkcja tworząca "pudełka" produktów pobranych z serwera
     */
    private void loadProducts(){



        try{

            ProductDto[] products = service.getProducts();


            setsFlow.getChildren().clear();
            pizzaFlow.getChildren().clear();
            drinksFlow.getChildren().clear();

            if(dragBox!=null){
                mainPane.getChildren().remove(dragBox);
            }

            for(ProductDto dto : products){

                switch (dto.getType()) {
                    case "zestaw" -> setsFlow.getChildren().add(createBox(dto));
                    case "pizza" -> pizzaFlow.getChildren().add(createBox(dto));
                    case "napoj" -> drinksFlow.getChildren().add(createBox(dto));
                }
            }

            dragBox = new VBox();

            dragBox.setPrefSize(50, 50);
            dragBox.setStyle("-fx-background-color: WHITE");
            dragBox.setAlignment(Pos.CENTER);
            dragBox.setBorder(boxBorder);
            dragBox.setCursor(Cursor.CLOSED_HAND);
            mainPane.getChildren().add(dragBox);
            dragBox.setDisable(true);
            dragBox.setVisible(false);
            dragBox.setPickOnBounds(false);
            Label plusLabel = createLabel("+", 20);
                plusLabel.setPickOnBounds(false);
            dragBox.getChildren().add(plusLabel);
        }
        catch(IOException e){
            header.setText("Błąd połączenia z serwerem... ");
        }

    }

    /**
     * Funkcja obsługująca zdarzenia związane z elementami w koszyku
     * @param event zdarzenie typu MouseEvent
     */
    private void basketItemHandler(MouseEvent event){

        HBox box = ((HBox)event.getSource());
        Pane parent = ((Pane)box.getParent());
        if(event.getEventType() == MouseEvent.MOUSE_ENTERED)
            box.getChildren().get(0).setVisible(true);
        else if(event.getEventType() == MouseEvent.MOUSE_EXITED)
            box.getChildren().get(0).setVisible(false);
        else if(event.getEventType() == MouseEvent.MOUSE_CLICKED){
            parent.getChildren().remove(box);
            refreshBasket();
        }

    }

    /**
     * Funkcja obsługująca zdarzenia związane z elementami w koszyku podczas wyboru darmowych napoi
     * @param event zdarzenie typu MouseEvent
     */
    private void basketFreeDrinkHandler(MouseEvent event){

        HBox box = ((HBox)event.getSource());
        ProductDto boxDto = ((ProductDto)box.getUserData());
        Pane parent = ((Pane)box.getParent());
        if(event.getEventType() == MouseEvent.MOUSE_ENTERED)
            box.getChildren().get(0).setVisible(true);
        else if(event.getEventType() == MouseEvent.MOUSE_EXITED)
            box.getChildren().get(0).setVisible(false);
        else if(event.getEventType() == MouseEvent.MOUSE_CLICKED){
            parent.getChildren().remove(box);
            refreshBasket();
            freeDrinksLeft+=boxDto.getAmount();
            header.setText("Wybierz darmowe napoje, pozostało ci: "+freeDrinksLeft);
        }

    }

    /**
     * Funkcja pomocnicza tworząca element prezentujący produkt, wyświetlany w koszyku
     * @param dto Obiekt klasy ProductDto opisujący produkt
     * @return Obiekt klasy HBox prezentujący produkt w koszyku
     */
    private HBox createBasketItem(ProductDto dto){



        dto.setAmount(1);


        HBox box = new HBox();
        FlowPane.setMargin(box, boxMargin);
        box.setPrefSize(230, 50);
        box.setAlignment(Pos.CENTER_LEFT);
        box.setPadding(boxPadding);
        box.setUserData(dto);



        if(choosingFreeDrinks){
            box.setOnMouseEntered(this::basketFreeDrinkHandler);
            box.setOnMouseExited(this::basketFreeDrinkHandler);
            box.setOnMouseClicked(this::basketFreeDrinkHandler);
        }
        else{
            box.setOnMouseEntered(this::basketItemHandler);
            box.setOnMouseExited(this::basketItemHandler);
            box.setOnMouseClicked(this::basketItemHandler);
        }


        box.setCursor(Cursor.HAND);

        box.setStyle("-fx-background-color: WHITE;"+
                     "-fx-border-color: LIGHTGREY;"+
                     "-fx-border-width: 1 0");



        Label delete = createLabel("X  ", 15);
            delete.setStyle("-fx-text-fill: RED");
            delete.setVisible(false);

        Label name = createLabel(dto.getName(), 13);
            HBox.setMargin(name, new Insets(0,5,0,0));
            name.setWrapText(true);
            name.setMaxWidth(100);
            name.setPrefWidth(100);



        Label amount = createLabel(" x "+dto.getAmount(), 13);
            HBox.setMargin(amount, new Insets(0,10,0,0));

        Label price = createLabel(createPriceStr(dto.getPrice()*dto.getAmount()), 12);



        box.getChildren().add(delete);
        box.getChildren().add(name);
        box.getChildren().add(amount);
        box.getChildren().add(price);


        return box;

    }

    /**
     * Funkcja aktualizująca wyświetlane w koszyku elementy przy interakcji użytkownika
     */
    private void refreshBasket(){



        double price = 0.0;


        int pizzas = 0;

        for(Node box : basketPane.getChildren()){

            HBox thisBox = ((HBox)box);
            ProductDto boxDto = ((ProductDto)thisBox.getUserData());
            Label amountLabel = ((Label)thisBox.getChildren().get(2));
            Label priceLabel = ((Label)thisBox.getChildren().get(3));

            amountLabel.setText(" x "+boxDto.getAmount()+" ");

            priceLabel.setText(createPriceStr(boxDto.getPrice()*boxDto.getAmount()));

            price+=boxDto.getPrice()*boxDto.getAmount();

            if(boxDto.getType().equals("pizza")|| boxDto.getType().equals("5050"))
                pizzas+=boxDto.getAmount();

        }



        if((pizzas/2) > 0 && !choosingFreeDrinks)
            orderButton.setText("Wybierz darmowe napoje");



        else
            orderButton.setText("Zamów");


        if(price > 100.0)
            priceLabel.setText(createPriceStr(price-((price/100.0)*20))+" (po obniżce) ");

        else
            priceLabel.setText(createPriceStr(price));



    }

    /**
     * Funkcja dodająca nowy element do koszyka
     * @param dto Obiekt klasy ProductDto opisujący produkt
     */
    private void addToBasket(ProductDto dto){





        for(Node box : basketPane.getChildren()){
            HBox thisBox = ((HBox)box);
            ProductDto boxDto = ((ProductDto)thisBox.getUserData());

            if(dto.getId()>0 && boxDto.getId() == dto.getId() && boxDto.getType().equals(dto.getType())){
                boxDto.setAmount(boxDto.getAmount()+1);
                refreshBasket();
                return;
            }

            else if(dto.getId() < 0 &&  boxDto.getOptional1() == dto.getOptional1() && boxDto.getOptional2() == dto.getOptional2()){
                boxDto.setAmount(boxDto.getAmount()+1);
                refreshBasket();
                return;
            }

        }

        basketPane.getChildren().add(createBasketItem(dto));
        refreshBasket();

    }

    /**
     * Funkcja inicjująca stan wyboru darmowych napoi oraz wysyłająca zamówienie do serwera
     */
    @FXML
    private void sendBasket(){




        if(basketPane.getChildren().isEmpty())
            return;


        int pizzas = 0;

        ArrayList<ProductDto> products = new ArrayList<>();

        for(Node box : basketPane.getChildren()){
            HBox thisBox = ((HBox)box);
            ProductDto boxDto = ((ProductDto)thisBox.getUserData());
            if(boxDto.getType().equals("pizza")|| boxDto.getType().equals("5050"))
                pizzas+=boxDto.getAmount();
            products.add(boxDto);
        }

        int freeDrinks = pizzas/2;

        if(freeDrinks == 0 || choosingFreeDrinks){

            try{

                byte[] receiptFile = service.sendProducts(products);

                FXMLLoader loader = new FXMLLoader(App.class.getResource( "/end.fxml"));
                App.getScene().setRoot(loader.load());
                EndController controller = loader.getController();

                controller.init(receiptFile);


            }

            catch(Error e){
                header.setText("Błąd wewnętrzny... Nie udało się wykonać zamówienia");
            }

            catch(IOException e){
                header.setText("Błąd połączenia... Nie udało się wykonać zamówienia");
            }

        }

        else{

            for(Node box : setsFlow.getChildren()) {
                VBox thisBox = ((VBox) box);
                thisBox.setDisable(true);
            }

            for(Node box : pizzaFlow.getChildren()) {
                VBox thisBox = ((VBox) box);
                thisBox.setDisable(true);
            }

            for(Node box : basketPane.getChildren()) {
                HBox thisBox = ((HBox) box);
                thisBox.setDisable(true);
            }


            for(Node box : drinksFlow.getChildren()) {
                box.setOnMouseEntered(this::freeDrinkHandler);
                box.setOnMouseExited(this::freeDrinkHandler);
                box.setOnMouseClicked(this::freeDrinkHandler);
                box.setOnDragDetected(null);
                box.setOnMouseDragged(null);
                box.setOnMouseReleased(null);
            }

            header.setText("Wybierz darmowe napoje, pozostało ci: "+freeDrinks);


            orderButton.setText("Zamów");

            freeDrinksLeft = freeDrinks;

            choosingFreeDrinks = true;

        }


    }

    /**
     * Funkcja usuwająca elementy z koszyka
     */
    @FXML
    private void clearBasket(){


        basketPane.getChildren().clear();
        dragging = false;
        choosingFreeDrinks = false;
        freeDrinksLeft = 0;
        loadProducts();
        refreshBasket();
        header.setText("Wybierz swój posiłek:");
    }

}
